// importe le module Express
const express = require('express');
const app = express();

// on utilisera le port 3000 pour accéder au serveur
const port = 3000;

// définition d'une route GET sur l'URL racine ('/')
app.get('/', (req, res) => {
    // envoie une réponse 'Hello World!' au client
    res.send('Hello World!');
})
// définition d'une route GET sur l'URL racine ('/menu')
app.get('/menu', (req, res) => {
    // envoie une réponse  au client
    res.send('Bienvenue');
});

// définition d'une route GET sur l'URL racine ('/data')
app.get('/data', (req, res) => {
    // envoie une réponse  au client
    res.send({ name: 'Jean-Bernard', power: 1520, life: 500000 });
});

app.get('/menu', (req, res) => {
    // envoie une réponse  au client
    res.send('Bienvenue');
});
// démarrage du serveur sur le port défini
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})

//Dans cet exemple, lorsque le serveur reçoit une requête GET à l'adresse "/", il répond avec le texte 'Hello World!'.
// Il faudra pour cela lancer le serveur avec la commande node index.js.